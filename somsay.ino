#include <Servo.h>

// Define the servo motor
Servo myservo;

// Define the analog pin for the potentiometer
const int potPin = A0;

// Define a variable to store the potentiometer value
int potValue = 0;

void setup() {
  // Attach the servo to pin 9
  myservo.attach(9);
  
  // Begin serial communication for debugging
  Serial.begin(9600);
}

void loop() {
  // Read the value from the potentiometer
  potValue = analogRead(potPin);

  // Map the potentiometer value (0-1023) to the servo angle (0-180)
  int servoAngle = map(potValue, 0, 1023, 0, 180);

  // Write the mapped value to the servo
  myservo.write(servoAngle);

  // Print the potentiometer and servo values to the serial monitor
  Serial.print("Potentiometer Value: ");
  Serial.print(potValue);
  Serial.print(" -> Servo Angle: ");
  Serial.println(servoAngle);

  // Small delay for stability
  delay(15);
}